/* 
 * File:   Level.h
 * Author: aleksandra
 *
 * Created on 5 maj 2012, 16:44
 */

#ifndef LEVEL_H
#define	LEVEL_H
#include <vector>
#include <string>
#include "Beta.h"
#include "Exception.h"
#include "Histogram.h"

class Level {
public:

    Level(double energy, int atomicNumber);
    virtual ~Level();
    void SetBetaFeedingFunction (double betaFeedingFun) {betaFeedingFunction_ = betaFeedingFun;}
    void SetMaxBetaEnergy(double maxBetaEnergy)
	{
		maxBetaEnergy_ = maxBetaEnergy;
		SetBetaMinus();
	}
	void SetBetaDecayProbability(double betaDecayProbability){betaDecayProbability_ = betaDecayProbability;}
	void SetBetaMinus();
	void SetBetaPlus(double bPlInt, double ecInt);
    void SetHalfLifeTime(std::string T12);

    double GetEnergy(){return energy_;}
    double GetBetaFeedingFunction(){return betaFeedingFunction_;}
    double GetMaxBetaEnergy(){return maxBetaEnergy_;}
    double GetT12() {return halfLifeTimeInSeconds_;}
	double GetBetaDecayProbability(){return betaDecayProbability_;}
		
	Histogram* GetNeutrinoSpectrum();
	Histogram* GetBetaSpectrum();
	
	Beta* GetBeta()
	{
		if (beta_)
			return beta_;
		else
			throw Exception("Level::GetBeta(): no beta in level!")	;
	}

private:
    double energy_; 
    double betaFeedingFunction_;
    
    int atomicNumber_;
    double maxBetaEnergy_;
    double halfLifeTimeInSeconds_;
	double betaDecayProbability_;//beat decay from the level - in case of isomer state
    Histogram* neutrinoSpectrum_;
    Histogram* betaSpectrum_;
	Beta* beta_;
};

#endif	/* LEVEL_H */

