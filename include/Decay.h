/* 
 * File:   Decay.h
 * Author: aleksandra
 *
 */

#ifndef DECAY_H
#define	DECAY_H
#include <string>
#include <vector>
#include "Histogram.h"
#include "Level.h"
class Decay
{
public:
    Decay(int atomicMass, int atomicNumber, double qVal, std::vector<Level*> levels, Level* parentLevel);
    Decay(const std::string& ensdfFilename);
    virtual ~Decay();
    Histogram* GetNeutrinoSpectrum();
    Histogram* GetBetaSpectrum();


private:

    int atomicMass_;
    int atomicNumber_;
    double qVal_;
    std::vector<Level*> levels_;
    Level* parentLevel_;
    static Level* isomerLevel_;
	
	void NormalizeBetaFeedingDistribution();
	void MakeBetaAndNeutrinoSpectrum();
	Histogram* neutrinoSpectrum_;
	Histogram* betaSpectrum_;	

};

#endif	/* DECAY_H */

