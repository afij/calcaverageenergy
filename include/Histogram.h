/* 
 * File:   Histogram.h
 * Author: aleksandra
 *
 * Created on 5 Sep 2012, 16:40
 * Wrapper for TH1F
 */

#ifndef HISTOGRAM_H
#define	HISTOGRAM_H
#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include "Exception.h"

typedef std::vector<float> vecFloat;

class Histogram
{
	public:
	Histogram( const Histogram* hist );
	/** @param data - vector of floats included data for histogram. It is not
	peaks hights! **/
	Histogram(float xMin, float xMax, vecFloat &data);
	/** Static method gived instance of empty histogram **/
	static Histogram *GetEmptyHistogram(float xMin = 0., float xMax = 10., int nrOfBins = 10);	
  ~Histogram()
  {
  	//std::cout<<" dest "<<--nrOfhist<<std::endl;
  	nrOfCounts_.clear();
  	energy_.clear();
  }

//Getters:	
	int GetNrOfBins(){return nrOfCounts_.size();}
	float GetBinValue(int binNr);
	double GetXMin(){return (*energy_.begin());}
	double GetXMax(){return energy_.back();}
	
	float GetNrOfCounts()
	{
		float totNrOfCounts = 0;
		for(unsigned int i=0; i<nrOfCounts_.size(); ++i)
			totNrOfCounts += nrOfCounts_.at(i);
		return totNrOfCounts;
	}
	
	float GetBinCenter(int binNr){
		if (binNr>=nrOfCounts_.size())
			throw Exception("Histogram::GetBinCenter: binNr > maxBin ");
		return (energy_.at(binNr+1) + energy_.at(binNr))/2.;
	}	
	
	vecFloat GetAllData() {return nrOfCounts_;}
	std::vector<int> GetAllIntData()
	{
		std::vector<int> allData(nrOfCounts_.size());
		for(int i=0; i<nrOfCounts_.size(); i++)
			allData.at(i) = static_cast<int> (nrOfCounts_.at(i));
		return allData;
	}
	
	vecFloat GetEnergyVector(){return energy_;}
	
	int FindBin(double energy)
	{
		if(energy >= energy_.back())
			throw Exception("Histogram::FindBin: energy >= maxEnergy ");
		for(unsigned int i=0; i<energy_.size()-1; ++i)
			if(energy >= energy_.at(i) && energy < energy_.at(i+1))
				return i;
	}
	
	Histogram* Convolution(Histogram* hist2);
	/** rebin histogram "group" times
	*example: if group = 5 five bins in old histograms become one
	**/
	void Rebin(float group);
	
	/** newEnergy = sum_i (calParam.at(i) * oldEnergy^(i)) 
	example: if calParam has 3 numbers calibrated energy is:
	newEn = calParam.at(0)*oldEn^0 + calParam.at(1)*oldEn^1 + calParam.at(2)*oldEn^2
	**/	
	void Recalibrate(std::vector<float> &calParam);

	void Scale(float c= 1.)
	{
		for(unsigned int i=0; i<nrOfCounts_.size(); ++i)
			nrOfCounts_.at(i) *= c;
	}
	void Normalize(double c = 1.) {this->Scale(c/GetNrOfCounts()); }
	void Fill(float energy, float peakHight)
	{
		int binNumber = FindBin(energy);
		nrOfCounts_.at(binNumber) += peakHight;
	}
	
	/** Adding method, works only if energy per channel is equal for both histograms**/
	void Add(Histogram* hist, int weight);
	void Add(Histogram* hist, double weight);

	private:
	/** obligatory **/
	vecFloat nrOfCounts_;
	/**left edges of each bin
	*  size of energy_ vector is always one higher then nrOfCounts_ vector
	**/
	vecFloat energy_;
	std::string name_;
	std::string title_;
	float FindNewEnergy(double oldEnergy,  std::vector<float> &calParam);
	vecFloat FindEnergyVector(float xMin, float xMax, int nrOfBins);
	
	
	static int nrOfhist;
};

#endif
