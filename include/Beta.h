/* 
 * COPY OF Beta.hh from geant class. Some changes were made.
 * File:   Beta.h
 * Author: aleksandra
 *
 * Created on 5 maj 2012, 16:44
 * Class represent beta transition (beta plus, minus and EC). 
 */

#ifndef BETA_H
#define	BETA_H
#include "Exception.h"
#include "FermiDistribution.h"
#include <string>
#include <iostream>
#include <assert.h>
#include <vector>
#include "Histogram.h"


class Beta {
public:

	Beta(double maxEnergy, int atomicNumber, int eCharge);
	virtual ~Beta();
	double GetMaxBetaEnergy(){return maxBetaEnergy_;}
	void SetBetaPlusECInt(double betaPlusInt, double ECInt){
	if(maxBetaEnergy_ < 0)
		betaPlusInt_ =0;
	else
		betaPlusInt_ = betaPlusInt;
		ECInt_ = ECInt;
	}
	
	Histogram* GetBetaSpectrum();
private:
	
	double maxBetaEnergy_;
	int atomicNumber_;
	int eCharge_;
    
	double betaPlusInt_;
	double ECInt_;
	FermiDistribution *betaEnergyDistribution_;
	Histogram* betaSpectrum_;
	
};

#endif	/* Beta_H */

