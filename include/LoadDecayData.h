#ifndef LOADDECAYDATA_H
#define	LOADDECAYDATA_H
#include "Level.h"
#include <vector>
#include <string>

class LoadDecayData {
public:
    LoadDecayData(const std::string filename);
    ~LoadDecayData();
    std::vector <Level*> GetLevels() {return levels_;}
    int GetAtomicMass() {return atomicMass_;}
    int GetAtomicNumber() {return atomicNumber_;}
    double GetQVal() {return qVal_;}
    Level* GetParentLevel() {return parentLevel_;}

private:
    std::vector <Level*> levels_;
    Level* parentLevel_;
    int atomicMass_;
    int atomicNumber_;
    double qVal_;

};

#endif	/* LOADDECAYDATA_H */
