/**
 *  Simple code calculating average gamma and beta energy per decay 
 *  as well as average number of antineutrino interaction with 
 *  the detector matter.
 *  Aleksandra Fijalkowska
 * */


#include "Histogram.h"
#include "Decay.h"
#include "MyTemplates.h"
#include <vector>
#include <string>
#include <fstream>

using namespace std;

float getInversbetaDecayCrossSect(float neutrinoEnergy);

int main(int argc, char** argv)
{
	ifstream inputfile;
	string inputfilename;
	if(argc==1)
	{
		cout<<"Input file name:";
		cin>>inputfilename;
		cout << inputfilename << endl;
	}
	else
	{
		inputfilename = argv[1];
		cout << inputfilename << endl;
	}
	
	inputfile.open(inputfilename.c_str());
	if(inputfile.is_open())
	{
		try
		{
			string inputFileName, outputFileName, temp;
			inputfile>>temp>>inputFileName>>temp>>outputFileName;	

			Decay* decay = new Decay(inputFileName);
			Histogram *betaSpec = decay->GetBetaSpectrum();
			Histogram *neutrinoSpec = decay->GetNeutrinoSpectrum();
			//std::cout<<betaSpec<<endl;
			vector<float> energy = betaSpec->GetEnergyVector();
			vector<float> betaSpecVal = betaSpec-> GetAllData();
			vector<float> neutrinoSpecVal = neutrinoSpec -> GetAllData();

			ofstream output;
			output.open(outputFileName.c_str());

			float reverseBetaProb = 0;
			const float threshold = 1806;
			//cout << "energy  betaInt(bI) neutrinoInt(nI) bI+nI crossSec*nI"<<endl;
			output << "energy  betaInt(bI) neutrinoInt(nI) bI+nI crossSec*nI"<<endl;
			float totalBetaEnergy = 0;
			for(unsigned int i = 0; i != betaSpecVal.size(); ++i)
			{	
				float energy = betaSpec->GetBinCenter(i);
				output << energy << " " << betaSpecVal.at(i) 
				<< " " << neutrinoSpecVal.at(i) << " " 
				<< betaSpecVal.at(i) + neutrinoSpecVal.at(i) << " " 
				<< getInversbetaDecayCrossSect(energy)*neutrinoSpecVal.at(i) << endl;
				//if(betaSpec->GetBinCenter(i) < threshold)
				//	sumNeutrinoEnBelowTh +=neutrinoSpecVal.at(i);
				reverseBetaProb += getInversbetaDecayCrossSect(energy)*neutrinoSpecVal.at(i);
				totalBetaEnergy += energy*betaSpecVal.at(i);
			}
			output.close();
			cout<<"nr of neutr interact " << reverseBetaProb*100. << " 10^{−43} cm^2 "<<endl;
			cout << "average beta energy: " << totalBetaEnergy << " keV" << endl;
		}
        catch(Exception e)
        {
            cout << "Exception " << e.GetMessage() << endl;
            //throw e;
        }
    }

	return 0;
	
}		



float getInversbetaDecayCrossSect(float neutrinoEnergy)
{
	/* cross section for invers beta decay abowe threshold tH = 1806 keV
	 * is calculated using quadratic
	 * function cs(E) = a*E^2 + b*E + c
	 * a, b, c parameters was calculadet using data from
	 * A. Strumia and F. Vissani paper PRB 564(2003) 42-54
	*/
	float energyInMev = neutrinoEnergy/1000.;
	const float a = 0.00871253;//10^(-41)*cm2/MeV^2
	const float b = -0.0196754;//10^(-41)*cm2/MeV
	const float c = 0.00722737;//10^(-41)*cm2
	const float threshold = 1.806;
	if(energyInMev < threshold)
		return 0.;
	else
		return a*energyInMev*energyInMev+b*energyInMev+c;
}
