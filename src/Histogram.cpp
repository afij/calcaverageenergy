
#include "Histogram.h"

using namespace std;


Histogram::Histogram( const Histogram* hist )
{
	nrOfCounts_.insert(nrOfCounts_.begin(), hist->nrOfCounts_.begin(), hist->nrOfCounts_.end());
	energy_ .insert(energy_.begin(), hist->energy_.begin(), hist->energy_.end());
	//std::cout<<"const "<<++nrOfhist<<std::endl;
}


Histogram::Histogram(float xMin, float xMax, vecFloat &data)
{
	for(int i=0; i<data.size(); i++)
		nrOfCounts_.push_back(data.at(i));
	energy_ = FindEnergyVector(xMin, xMax, data.size());
	//std::cout<<"const "<<++nrOfhist<<std::endl;
}

Histogram* Histogram::GetEmptyHistogram(float xMin, float xMax, int nrOfBins)
{
	vecFloat data;
	for(int i=0; i<nrOfBins; i++)
	{
			data.push_back(0);
	}
	return new Histogram(xMin, xMax, data);
}

float Histogram::GetBinValue(int binNr) {
	if(binNr < nrOfCounts_.size() && binNr >= 0)
		return nrOfCounts_.at(binNr);//first bin has nr 1
	else 
	{
		throw Exception("Histogram::GetBinValue: binNr > maxBin ");
	}
}

vecFloat Histogram::FindEnergyVector(float xMin, float xMax, int nrOfBins)
{
	vecFloat energy;
	for(int i=0; i<nrOfBins+1; i++)
	{
		float leftEdgeOfBin = xMin + static_cast<float>((xMax-xMin)/nrOfBins * i);
		energy.push_back(leftEdgeOfBin);
	}
	return energy;
}

void Histogram::Add(Histogram* hist, int weight)
{
	this->Add(hist, static_cast<int>(weight));
}

void Histogram::Add(Histogram* hist, double weight)
{
	//first chcek if nr of keV per channel is the same for both histograms
	if(static_cast<float> ((this->GetXMax() - this->GetXMin())/this->GetNrOfBins())
		-static_cast<float> ((hist->GetXMax() - hist->GetXMin())/hist->GetNrOfBins()) > 1)
	{
		std::cout<<this->GetXMax()<<" "<<this->GetXMin()<<" "<<this->GetNrOfBins()<<" ";
		std::cout << static_cast<float> ((this->GetXMax() - this->GetXMin())/this->GetNrOfBins()) << " ";
		
		std::cout<<hist->GetXMax()<<" "<<hist->GetXMin()<<" "<<hist->GetNrOfBins()<<" ";
		std::cout<<static_cast<float> ((hist->GetXMax() - hist->GetXMin())/hist->GetNrOfBins())<<std::endl;
		throw WrongCalibrationException("Hisotogram::Add(Histogram*, int): Different calibration!");
	}
		
	float calFactor	=static_cast<float> ((this->GetXMax() - this->GetXMin())/this->GetNrOfBins());
	float xMin = (this->GetXMin() < hist->GetXMin()) ? this->GetXMin():hist->GetXMin();
	float xMax = (this->GetXMax() > hist->GetXMax()) ? this->GetXMax():hist->GetXMax();
	int nrOfBins = static_cast<int> (xMax - xMin)/calFactor + 1;
	
	Histogram* newHist = GetEmptyHistogram(xMin, xMax, nrOfBins);
	for(int i=0; i<this->GetNrOfBins(); i++)
		newHist->Fill(this->GetBinCenter(i), this->GetBinValue(i));
		
	for(int i=0; i< hist->GetNrOfBins(); i++)
		newHist->Fill(hist->GetBinCenter(i), hist->GetBinValue(i)*weight);
		
	nrOfCounts_.clear();
	nrOfCounts_ = newHist->GetAllData();
	energy_.clear();
	energy_ = newHist->GetEnergyVector();
	delete newHist;
}


Histogram* Histogram::Convolution(Histogram* hist2) {
		int n1 = this -> GetNrOfBins();
		int n2 = hist2 -> GetNrOfBins();
		float norm1 = this->GetNrOfCounts();
		float norm2 = hist2->GetNrOfCounts();
		
		if(norm1 == 0 || norm2 == 0)
			return GetEmptyHistogram(0, n1+n2, n1+n2);

		Histogram* conv = GetEmptyHistogram(0, n1+n2, n1+n2);
		float value;
		for(int i=0; i< n1+n2; i++)
		{
			value = 0;
			for(int j=0; j<=i; j++)
			{
				if(j<n1 && i-j < n2)
					//value += this -> GetBinValue(j)/norm1 *hist2 -> GetBinValue(i-j)/norm2;
					value += this -> GetBinValue(j) *hist2 -> GetBinValue(i-j);
				//if(i==0 && j==0)
				//	cout<<"value in 0 bin: "<<value<<endl;	
			}
			if(value < 0)
				cout<<" Bin value lower than zero "<<value<<endl;
			conv -> Fill(i, value);
		}
		return conv;
}


void Histogram::Rebin(float factor)
{
	vecFloat calFactor;
	calFactor.push_back(0);
	calFactor.push_back( 1./factor);
	Recalibrate(calFactor);
	//change enrgy vector
	int oldNrOfBins = this->GetNrOfBins();
	int newNrOfBins = oldNrOfBins/factor;
	double xMin = this->GetXMin();
	double xMax = this->GetXMax();
	energy_.clear();
	energy_ = FindEnergyVector(this->GetXMin(),this->GetXMax(), newNrOfBins);
	cout<<energy_.size()<<" "<<nrOfCounts_.size()<<endl;
}

void Histogram::Recalibrate(std::vector<float> &calParam)
{
	int oldNrOfBins = this->GetNrOfBins();
	double xMinOld = this->GetXMin();
	double xMaxOld = this->GetXMax();
	double binLength = (xMaxOld-xMinOld)/oldNrOfBins;
	
	Histogram* newSpectrum = GetEmptyHistogram(xMinOld, xMaxOld, oldNrOfBins);

	for(int i=0; i<oldNrOfBins-1; i++)
	{
		/**take low and high edge of bin from old spectrum (enLow and enHigh)
		*  claculate calibrated energy (calEnLow and calEnHigh)
		*  chcek if both edges are in the same channel in new hist
		*  if yes add all counts to one channel 
		*  otherwise spread counts with the appropriate weights into several channels
		**/
		double enLow = energy_.at(i);
		double enHigh = energy_.at(i+1);
		double calEnLow = FindNewEnergy(enLow, calParam);
		double calEnHigh = FindNewEnergy(enHigh, calParam);
		if(calEnHigh >= xMaxOld)
			break;
			
		int calBinLow = newSpectrum->FindBin(calEnLow);
		int calBinHigh = newSpectrum->FindBin(calEnHigh);

		if (calBinLow == calBinHigh)
			newSpectrum->Fill(calEnLow,nrOfCounts_.at(i));
		else
		{
			long double firstBinFraction = (newSpectrum->energy_.at(calBinLow +1) - calEnLow)
			/(calEnHigh - calEnLow);
			newSpectrum->Fill(calEnLow, nrOfCounts_.at(i)*firstBinFraction);
			long double lastBinFraction = (calEnHigh - newSpectrum->energy_.at(calBinHigh))
			/(calEnHigh - calEnLow);
			newSpectrum->Fill(calEnHigh, nrOfCounts_.at(i)*lastBinFraction);


			int nrOfComplededBins =  calBinHigh - calBinLow - 1;
			if(nrOfComplededBins > 0)
			{
				long double restBinFraction = (1. - firstBinFraction - lastBinFraction)/
				(static_cast<double>(nrOfComplededBins));
				for(int k = 1; k<nrOfComplededBins+1; k++)
					newSpectrum->Fill(calEnLow + k*binLength, nrOfCounts_.at(i)*restBinFraction);
			}
		}	
	}	
	nrOfCounts_.clear();
	nrOfCounts_ = newSpectrum->GetAllData();
	energy_.clear();
	energy_ = newSpectrum->GetEnergyVector();
	delete newSpectrum;
	//return newSpectrum;
}

float Histogram::FindNewEnergy(double oldEnergy, vector<float> &calParam)
{
	float newEnergy = 0;
	for (unsigned int i = 0; i<calParam.size(); ++i)
		newEnergy += calParam.at(i)*pow(oldEnergy, i);
	if (newEnergy < 0)
		newEnergy = 0;
	return newEnergy;
}



int Histogram::nrOfhist = 0;
