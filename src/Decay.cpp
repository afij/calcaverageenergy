/* 
 * File:   Decay.cpp
 * Author: aleksandra
 * 
 * Created on 1 maj 2012, 20:16
 */
#include "Decay.h"
#include "LoadDecayData.h"
#include "Exception.h"
#include "MyTemplates.h"
#include <iostream>
#include <fstream>

Decay::Decay(int atomicMass, int atomicNumber, double qVal, std::vector<Level*> levels, Level* parentLevel) :
	atomicMass_(atomicMass), atomicNumber_(atomicNumber), qVal_(qVal), levels_(levels), parentLevel_(parentLevel)
{
	neutrinoSpectrum_ = 0;
	betaSpectrum_ = 0;
	NormalizeBetaFeedingDistribution();
	MakeBetaAndNeutrinoSpectrum();
}


Decay::Decay(const std::string &ensdfFilename)
{
	LoadDecayData* loadDecayData = new LoadDecayData(ensdfFilename);
	atomicMass_ = loadDecayData->GetAtomicMass();
	atomicNumber_ = loadDecayData->GetAtomicNumber();
	//atomicNumber_ = 0;
	qVal_ = loadDecayData->GetQVal();
	levels_ = loadDecayData->GetLevels();
	parentLevel_ = loadDecayData->GetParentLevel();
	
	neutrinoSpectrum_ = 0;
	betaSpectrum_ = 0;
	NormalizeBetaFeedingDistribution();
	MakeBetaAndNeutrinoSpectrum();
}

Decay::~Decay()
{

	for(std::vector<Level*>::iterator it = levels_.begin(); it != levels_.end(); ++it)
		delete (*it);
	delete parentLevel_;
	if(neutrinoSpectrum_ != 0)
		delete neutrinoSpectrum_;
	if(betaSpectrum_ != 0)
		delete betaSpectrum_;
}

void Decay::NormalizeBetaFeedingDistribution()
{
	double maxBetaFeeding(0);
	double gammaEnergyPerDecay(0);
	double level0Feeding (0);
	
	for (unsigned int i = 0; i != levels_.size(); ++i)
	{
		maxBetaFeeding += levels_.at(i)->GetBetaFeedingFunction();
		if(levels_.at(i)->GetEnergy() - 0. < 0.01)// searching for level 0
			level0Feeding = levels_.at(i)->GetBetaFeedingFunction();
	}

	if(maxBetaFeeding < 1e-5)
	{
		maxBetaFeeding = 1;
		//throw Exception("Decay::SetBetaFeedingDistribution() No beta feedings in ensdf file");
	}
	for (unsigned int i = 0; i != levels_.size(); ++i)
	{
		double betaInt = levels_.at(i)->GetBetaFeedingFunction();
		levels_.at(i)->SetBetaFeedingFunction(betaInt/maxBetaFeeding);
		gammaEnergyPerDecay += (betaInt/maxBetaFeeding)*levels_.at(i)->GetEnergy();
		cout<<"level: "<<levels_.at(i)->GetEnergy() << " int: "<< levels_.at(i)->GetBetaFeedingFunction()<<std::endl;
	}		
	
	std::cout << "Mean gamma energy per dacay: " << gammaEnergyPerDecay << std::endl;
	std::cout << "Level 0 feeding: " << level0Feeding/maxBetaFeeding << std::endl;
}

/*void Decay::MakeBetaAndNeutrinoSpectrum()
{
	std::cout<<"levels.size="<<levels_.size()<<std::endl;
	Histogram* sampleSpectrum = levels_.at(0)->GetNeutrinoSpectrum();
	
	double eMin = sampleSpectrum->GetXMin();
	double eMax = sampleSpectrum->GetXMax();
	int nrOfChannels = sampleSpectrum->GetNrOfBins();
	
	neutrinoSpectrum_ = Histogram::GetEmptyHistogram(eMin, eMax, nrOfChannels);
	betaSpectrum_ = Histogram::GetEmptyHistogram(eMin, eMax, nrOfChannels);
	
	Histogram* sampleBetaSpectrum = levels_.at(0)->GetBetaSpectrum();

	for (unsigned int i = 0; i != levels_.size(); ++i)
	{
		
		double levelInt = levels_.at(i)->GetBetaFeedingFunction();
		neutrinoSpectrum_->Add(levels_.at(i)->GetNeutrinoSpectrum(), levelInt);
		betaSpectrum_->Add(levels_.at(i)->GetBetaSpectrum(), levelInt);
	}
}*/


void Decay::MakeBetaAndNeutrinoSpectrum()
{

	std::cout<<"levels.size= "<<levels_.size()<<std::endl;
	//cout << levels_.at(0)-> GetEnergy() << endl;
	Histogram* sampleSpectrum = levels_.at(0)->GetNeutrinoSpectrum();
	//std::cout << " ....." << endl;
	double eMin = sampleSpectrum->GetXMin();
	double eMax = sampleSpectrum->GetXMax();
	int nrOfChannels = sampleSpectrum->GetNrOfBins();
	vector<float> resForBeta(nrOfChannels, 0.);
	vector<float> resForNeutrino(nrOfChannels, 0.);
	//cout << "energy  betaInt(bI) neutrinoInt(nI) bI+nI crossSec*nI"<<endl;
	for (unsigned int i = 0; i != levels_.size(); ++i)
	{
		
		double levelInt = levels_.at(i)->GetBetaFeedingFunction();
		if(levelInt < 1e-7)
			continue;
		vector<float > betaSpec = levels_.at(i)->GetBetaSpectrum()->GetAllData();
		vector<float> neutrinoSpec = levels_.at(i)->GetNeutrinoSpectrum()->GetAllData();
		for(int chan = 0; chan != betaSpec.size(); ++chan)
		{
			resForNeutrino.at(chan) += neutrinoSpec.at(chan) * levelInt;
			resForBeta.at(chan) += betaSpec.at(chan) * levelInt;
		}
	}
	neutrinoSpectrum_ = new Histogram (eMin, eMax, resForNeutrino);
	betaSpectrum_ = new Histogram (eMin, eMax, resForBeta);
}


Histogram* Decay::GetNeutrinoSpectrum()
{
	return neutrinoSpectrum_;	
}

Histogram* Decay::GetBetaSpectrum()
{
	return betaSpectrum_;
}
Level* Decay::isomerLevel_ = 0L;
