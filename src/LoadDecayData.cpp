#include "LoadDecayData.h"
#include "PeriodicTable.h"
#include "MyTemplates.h"
#include "Exception.h"
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>

using namespace std;

LoadDecayData::LoadDecayData(const string filename)
{
    cout<<"loading"<<endl;
    string atomicName;
    ifstream in(filename.c_str());
    if (!in.is_open())
        throw IOException("Warning message: The file " + (string) filename + " is not open!");

    string data, buff;
    getline(in, buff);//first line - some information about decay
    getline(in, buff);

    data = buff.substr(0, 3);
    atomicMass_ = string2num <int> (data, std::dec);

    data = buff.substr(3, 2);
    atomicName = data;
    atomicNumber_ = PeriodicTable::GetAtomicNumber(atomicName);

    cout<<atomicMass_<<" "<<atomicName<<endl;

    while (!in.eof())
    {
        getline(in, buff);
		//cout<<buff<<endl;
        if (buff.size() < 80)
        break;

        if (buff[6]!='c')
        {
            if(buff[7]=='P')//parent level
            {
                data=buff.substr(9,10);
                double levelEnergy = string2num <double>(data, std::dec);
                parentLevel_ = new Level(levelEnergy, atomicNumber_);

                data=buff.substr(39,9);
                string halfLife = data;
                parentLevel_->SetHalfLifeTime(halfLife);

                data=buff.substr(64,10);
                qVal_ = string2num <double>(data, std::dec);
                if(qVal_<10)//in some cases Q is given in MeV
                    qVal_ = qVal_*1000;
                qVal_ += levelEnergy;
                std::cout<<"Q: "<<qVal_<<std::endl;
            }

            if(buff[7]=='L' && buff[5]==' ' && buff[6]==' ') //each level
            {
                data = buff.substr(9, 10);
                double energyLevel = string2num <double>(data, std::dec);
                levels_.push_back(new Level(energyLevel, atomicNumber_));

                data=buff.substr(39,9);
                string T12 = data;
                (*(levels_.end() - 1))->SetHalfLifeTime(T12);
                (*(levels_.end() - 1))->SetMaxBetaEnergy(qVal_ - energyLevel);
            }

            //beta minus
            if(buff[7]=='B' && buff[5]==' ' && buff[6]==' ' && levels_.size() > 0)
            {
                data = buff.substr(21, 8);
                double betaIntensity = string2num <double>(data, std::dec);
                (*(levels_.end() - 1))->SetBetaFeedingFunction(betaIntensity);
                (*(levels_.end() - 1))->SetBetaMinus();
            }

            //beta plus
            if(buff[7]=='E'&& buff[5]==' ' && buff[6]==' ' && levels_.size() > 0)
            {
                data = buff.substr(21, 8);
                double betaPlusIntensity = string2num <double>(data, std::dec);
                data=buff.substr(31,8);
                double ECIntensity= string2num <double>(data, std::dec);
                data=buff.substr(64,10);
                (*(levels_.end() - 1))->SetBetaFeedingFunction(betaPlusIntensity+ECIntensity);
                (*(levels_.end() - 1))->SetBetaPlus(betaPlusIntensity, ECIntensity);
            }

            //electron conversion coefficients

           }
        }
    in.close();
	cout<<"file loaded"<<endl;
}



LoadDecayData::~LoadDecayData()
{

}
