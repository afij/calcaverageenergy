/* 
 * File:   Beta.cpp
 * Author: aleksandra
 * 
 * Created on 5 maj 2012, 16:44
 */
#include "Beta.h"
#include <assert.h>

Beta::Beta(double maxEnergy, int atomicNumber, int eCharge):
maxBetaEnergy_(maxEnergy), atomicNumber_(atomicNumber), eCharge_(eCharge)
{
	if(maxEnergy > 0)
	{
		if(eCharge_ == -1 || eCharge_ == 1)
    	betaEnergyDistribution_ = new FermiDistribution(atomicNumber_, maxBetaEnergy_, eCharge_);
  	else
  		throw Exception("Beta::Beta(double maxEnergy, int atomicNumber, int eCharge) Wrong eCharge value");
  }
	betaSpectrum_ = 0;
}

Beta::~Beta()
{
	if(betaEnergyDistribution_ != 0L)
		delete betaEnergyDistribution_;
	if(betaSpectrum_ != 0)
		delete betaSpectrum_;

}


Histogram* Beta::GetBetaSpectrum()
{
	if(betaSpectrum_ != 0)
		return betaSpectrum_;
		
	std::vector<float> energy = betaEnergyDistribution_->GetEnergyVector();
	std::vector<float> probability = betaEnergyDistribution_->GetProbabilityVector();
	assert(energy.size() == probability.size() && energy.size() != 0);
	float eMin = energy.at(0);
	float eMax = energy.back();
	
	//std::cout<<" beta xMin: " << eMin << " xMax: " << eMax <<std::endl;
	betaSpectrum_ = new Histogram(eMin, eMax, probability);
	betaSpectrum_->Normalize(1.);
	return betaSpectrum_;
}


