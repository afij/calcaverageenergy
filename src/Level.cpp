/* 
 * File:   Level.cpp
 * Author: aleksandra
 * 
 * Created on 5 maj 2012, 16:44
 */

#include "Level.h"
#include <sstream>
#include <string>
#include <iostream>
#include <assert.h>


Level::Level(double energy, int atomicNumber): energy_(energy)
{
	beta_ = 0L;
	betaFeedingFunction_ = 0;
	neutrinoSpectrum_ = 0;
	betaSpectrum_ = 0;
	
	atomicNumber_ = atomicNumber;
	
	//atomicNumber_ = 0;
	
}


Level::~Level()
{
	if(beta_)
		delete beta_;
}

void Level::SetHalfLifeTime(std::string T12)
{
    std::stringstream iss(T12);
    double halfLifeTimeValue = 0.0;
    std::string halfLifeTimeUnit;
    iss >> halfLifeTimeValue;
    if (halfLifeTimeValue == 0.0)
    {
			halfLifeTimeInSeconds_ = 0.;
    }
    else
    {
			iss >> halfLifeTimeUnit;

			if(halfLifeTimeUnit == "F")
			{
	   		 halfLifeTimeInSeconds_ = halfLifeTimeValue*pow(10., -9.);
			}
			else if(halfLifeTimeUnit == "MS")
			{
					halfLifeTimeInSeconds_ = halfLifeTimeValue * 0.001;
			}
			else if(halfLifeTimeUnit == "US")
			{
					halfLifeTimeInSeconds_ = halfLifeTimeValue*pow(10., -6.);
			}
			else if(halfLifeTimeUnit == "S")
			{
			    halfLifeTimeInSeconds_ = halfLifeTimeValue;
			}
			else if(halfLifeTimeUnit == "M")
			{
				halfLifeTimeInSeconds_ = halfLifeTimeValue * 60.;
			}
			else if (halfLifeTimeUnit == "H")
			{
	   	 halfLifeTimeInSeconds_ = halfLifeTimeValue * 3600.;
			}
			else if (halfLifeTimeUnit == "Y")
			{
	   		 halfLifeTimeInSeconds_ = halfLifeTimeValue * 31556926.;
			}
			else
			{
				halfLifeTimeInSeconds_ = 0;
		//throw Exception("Parse faild: unknown time unit", Exception::ERROR_UNKNOWN_TIME_UNIT);
			}
    }
}



void Level::SetBetaMinus()
{

	if(maxBetaEnergy_ -0. <1e-10)
		throw Exception("Level::SetBetaMinus() Set qVal before");
	if(beta_ != 0L)
	{
		delete beta_;
	}
		
	beta_ = new Beta(maxBetaEnergy_, atomicNumber_, -1);
}

void Level::SetBetaPlus(double bPlInt, double ecInt)
{
	if(fabs(maxBetaEnergy_ - 0.) < 1e-10)
		throw Exception("Level::SetBetaPlus() Set qVal before");
	if(beta_ != 0L)
	{
		delete beta_;
	}
		
	beta_ = new Beta(maxBetaEnergy_-1022., atomicNumber_, 1);		
	beta_ ->SetBetaPlusECInt(bPlInt, ecInt);
}

Histogram* Level::GetNeutrinoSpectrum()
{
	std::cout<<" getNeutrinoSpec "<<energy_<<" ";
	if(neutrinoSpectrum_ != 0)
		return neutrinoSpectrum_;
	Histogram* betaSpectrum = beta_->GetBetaSpectrum();
	float eMin = betaSpectrum->GetXMin();
	float eMax = betaSpectrum->GetXMax();
	int nrOfChannels = betaSpectrum -> GetNrOfBins();
	std::vector<float> betaValues = betaSpectrum ->GetAllData();
	
	std::vector<float> neutrinoValues(nrOfChannels, 0.);
	for(unsigned int i=0; i !=betaValues.size(); ++i )
		neutrinoValues.at(i) = betaValues.at(nrOfChannels - i -1);
		
	neutrinoSpectrum_ = new Histogram(eMin, eMax, neutrinoValues);
	std::cout<<"size: "<<neutrinoSpectrum_->GetNrOfBins()<<std::endl;
	//neutrinoSpectrum_->Normalize(1.);
	return neutrinoSpectrum_;
}

Histogram* Level::GetBetaSpectrum()
{
	return beta_ ->GetBetaSpectrum();
}
