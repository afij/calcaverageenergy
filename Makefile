DESTDIR=.
CINCLUDEDIRS = -Iinclude
CPPFLAGS += $(CINCLUDEDIRS)

SRC = src/
neutrinoSpec: Beta.o Decay.o FermiDistribution.o Exception.o \
Histogram.o Level.o LoadDecayData.o PeriodicTable.o neutrinoSpec.o
	g++ $(LDFLAGS) Beta.o Decay.o FermiDistribution.o Exception.o PeriodicTable.o \
	Histogram.o Level.o LoadDecayData.o neutrinoSpec.o -o $(DESTDIR)/neutrinoSpec_Z

Beta.o: $(SRC)Beta.cpp
	g++ $(CPPFLAGS) -c $(SRC)Beta.cpp

Decay.o: $(SRC)Decay.cpp
	g++ $(CPPFLAGS) -c $(SRC)Decay.cpp
	

FermiDistribution.o: $(SRC)FermiDistribution.cpp
	g++ $(CPPFLAGS) -c $(SRC)FermiDistribution.cpp	

Exception.o: $(SRC)Exception.cpp
	g++ $(CPPFLAGS) -c $(SRC)Exception.cpp
		
Histogram.o: $(SRC)Histogram.cpp
	g++ $(CPPFLAGS) -c $(SRC)Histogram.cpp	
		
Level.o: $(SRC)Level.cpp
	g++ $(CPPFLAGS) -c $(SRC)Level.cpp	
		
LoadDecayData.o: $(SRC)LoadDecayData.cpp
	g++ $(CPPFLAGS) -c $(SRC)LoadDecayData.cpp	

PeriodicTable.o: $(SRC)PeriodicTable.cpp
	g++ $(CPPFLAGS) -c $(SRC)PeriodicTable.cpp
			
neutrinoSpec.o: neutrinoSpec.cpp
	g++ $(CPPFLAGS) -c neutrinoSpec.cpp
		
clean:
	rm -f *.o *~	
